### Send Private Transactions

**Step 1**: Click the **Transfer** button on the right to start a private transaction

![](_media/Wanchain-Private-1.PNG)

**Step 2**: Click **Switch to Private** in the **From** field and enter a **Private address** in the **To** field. The Recipient will need to share their Private address beforehand. Enter the amount of WAN to send and click **SEND** to start the private transaction. 


![](_media/Wanchain-Private-2.PNG)

**Step 3**: Enter the **Password** for your account and click **SEND TRANSACTION** to send a private transaction. 

![](_media/Wanchain-Private-3.PNG)


**Step 4**: The latest transaction is displayed at the top of the **Transaction List**. 

![](_media/Wanchain-Private-4.PNG)

**Step 5**: On the **Wanchain Explorer**, the **Value**  and the **To** address are masked and hidden from the public.

![](_media/Wanchain-Private-5.PNG)


### Receive Private Transactions

**Step 1**: Click **Details** to view detailed account settings

![](_media/Wanchain-Private-6.PNG)


**Step 2**: Click **Get OTA** to begin the process to receive a private transaction.

![](_media/Wanchain-Private-7.PNG)

**Step 3**: Enter the **Password** for your Account and Click **OK**. 

![](_media/Wanchain-Private-8.PNG)

**Step 4**: Wait a few minutes for the transaction to be processed and click **Redeem**. 

![](_media/Wanchain-Private-10.PNG)

**Step 5**: Click **Redeem** to accept the transaction.

![](_media/Wanchain-Private-11.PNG)

**Step 6**: The transaction details are show under **Redeem from OTAs**

![](_media/Wanchain-Private-12.PNG)