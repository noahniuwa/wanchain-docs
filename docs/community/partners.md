# Partners

- [Ethereum Enterprise Alliance](https://entethalliance.org/)
- [Hyperledger](https://hyperledger.org)
- Blockchain Interoperability Alliance
- [BitTemple](https://bittemple.io/)
- [Longhash](https://www.longhash.com/)
- [FoundationX](https://www.foundationx.io/)
- [Wandarin](http://wandarin.org/?lang=en)
- [Portal Network](https://www.portal.network/)
- [Ledger Wallet](https://www.ledger.com/)
- [Trezor](https://trezor.io/)
- [Trust Wallet](https://trustwallet.com/)
- [Theia](http://www.thachain.org/)
- [Cryptocurve](https://cryptocurve.io/)
- [Kronos](https://kronostoken.com/) 
- [Pundi X](https://pundix.com/)
- [Loopring](https://loopring.org/)
- [0xcert](https://0xcert.org/)
- [Maker DAO](https://makerdao.com/en/)
- [Formosa Financial](https://www.formosa.financial/)
- [Token Loan](https://tokenloan.info/)
- [Neoplace](https://neoplace.io/)
- [Litex](http://litex.io/)
- [Chainlink](https://chain.link)
- Hui Mei Hui
- [iQunxing](https://www.iqunxing.com/)
- [Tsinghua X-Lab](http://www.x-lab.tsinghua.edu.cn/en/)
- [CAICT](http://www.caict.ac.cn/english/)
- [BBAA](http://www.bbaachina.org.cn/en/)



| **Milestone**  |**Date**   |**Status** | 
|:---|:---|:---|